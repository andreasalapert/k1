import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ColorSort {

   enum Color {red, green, blue}


   public static void main(String[] param) {

   }

   public static void reorder(Color[] balls) {
      int [] colorCount = new int[3];
      int colorIndexInEnum;
      for(int i = 0; i < balls.length; i++){
         colorIndexInEnum = balls[i].ordinal();
         colorCount[colorIndexInEnum]++;
      }

      int redLastIndex = colorCount[0];
      int greenLastIndex = redLastIndex + colorCount[1];
      int blueLastIndex = redLastIndex + colorCount[1] + colorCount[2];

      Arrays.fill(balls, 0, redLastIndex, Color.red);
      Arrays.fill(balls, redLastIndex, greenLastIndex, Color.green);
      Arrays.fill(balls, greenLastIndex, blueLastIndex, Color.blue);


   }

}
